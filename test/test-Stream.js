import assert from "assert";
import Stream from "../lib/Stream";

suite("Stream", () => {
    suite("constructor", () => {
        test("should be run immediately", () => {
            let wasRun = false;
            new Stream((resolve, reject, emit) => {
                wasRun = true;
            });
            assert(wasRun);
        });

        test("should forward arguments", () => {
            new Stream((resolve, reject, emit, ...args) => {
                assert.deepEqual(args, [1, 2, 3]);
            }, 1, 2, 3);
        });

        suite("resolve(value)", () => {
            test("should resolve an unresolved stream", done => {
                const obj = {};

                new Stream((resolve, reject, emit) => {
                    resolve(obj);
                }).then(result => {
                    assert.strictEqual(obj, result);
                    done();
                });
            });

            test("should be run asynchronously", done => {
                const collected = [];

                new Stream((resolve, reject, emit) => {
                    collected.push(1);
                    resolve();
                    collected.push(2);
                }).then(() => {
                    assert.deepEqual(collected, [1, 2, 3]);
                }).then(done, done);

                collected.push(3);
            });

            test("should not resolve an already resolved stream", done => {
                const obj1 = {};
                const obj2 = {};

                new Stream((resolve, reject, emit) => {
                    resolve(obj1);
                    resolve(obj2);
                }).then(result => {
                    assert.strictEqual(obj1, result);
                    done();
                });
            });

            test("should not resolve an already rejected stream", done => {
                const original = new Error();

                new Stream((resolve, reject, emit) => {
                    reject(original);
                    resolve({});
                }).catch(err => {
                    assert.strictEqual(err, original);
                    done();
                });
            });

            test("should return true when called for an unresolved stream", done => {
                new Stream((resolve, reject, emit) => {
                    assert.strictEqual(resolve(), true);
                }).then(done, done);
            });

            test("should return false when called for a resolved stream", done => {
                new Stream((resolve, reject, emit) => {
                    resolve();
                    assert.strictEqual(resolve(), false);
                }).then(done, done);
            });

            test("should return false when called for a rejected stream", done => {
                new Stream((resolve, reject, emit) => {
                    reject(new Error());
                    assert.strictEqual(resolve(), false);
                }).catch(() => done());
            });
        });

        suite("reject(err)", () => {
            test("should reject an unresolved stream", done => {
                const original = new Error();

                new Stream((resolve, reject, emit) => {
                    reject(original);
                }).catch(err => {
                    assert.strictEqual(err, original);
                    done();
                });
            });

            test("should be run asynchronously", done => {
                const collected = [];

                new Stream((resolve, reject, emit) => {
                    collected.push(1);
                    reject(new Error());
                    collected.push(2);
                }).catch(() => {
                    assert.deepEqual(collected, [1, 2, 3]);
                }).then(done, done);

                collected.push(3);
            });

            test("should not reject an already resolved stream", done => {
                const obj = {};
                const original = new Error();

                new Stream((resolve, reject, emit) => {
                    resolve(obj);
                    reject(original);
                }).then(result => {
                    assert.strictEqual(obj, result);
                    done();
                });
            });

            test("should not reject an already rejected stream", done => {
                const err1 = new Error();
                const err2 = new Error();

                new Stream((resolve, reject, emit) => {
                    reject(err1);
                    reject(err2);
                }).catch(err => {
                    assert.strictEqual(err, err1);
                    done();
                });
            });

            test("should return true when called for an unresolved stream", done => {
                new Stream((resolve, reject, emit) => {
                    assert.strictEqual(reject(new Error()), true);
                }).catch(() => done());
            });

            test("should return false when called for a resolved stream", done => {
                new Stream((resolve, reject, emit) => {
                    resolve();
                    assert.strictEqual(resolve(), false);
                }).then(done, done);
            });

            test("should return false when called for a rejected stream", done => {
                new Stream((resolve, reject, emit) => {
                    reject(new Error());
                    assert.strictEqual(reject(new Error()), false);
                }).catch(() => done());
            });
        });

        suite("emit(value)", () => {
            test("should be run asynchronously", done => {
                const collected = [];

                new Stream((resolve, reject, emit) => {
                    collected.push(1);
                    emit(null);
                    collected.push(2);
                }).forEach(() => {
                    assert.deepEqual(collected, [1, 2, 3]);
                    done();
                });

                collected.push(3);
            });

            test("should be able to emit for an unresolved stream", done => {
                const collected = [];

                new Stream((resolve, reject, emit) => {
                    emit(1);
                    emit(2);
                    emit(3);
                    resolve();
                }).forEach(value => {
                    collected.push(value);
                }).then(() => {
                    assert.deepEqual(collected, [1, 2, 3]);
                }).then(done, done);
            });

            test("should not be able to emit for a resolved stream", done => {
                const collected = [];

                new Stream((resolve, reject, emit) => {
                    resolve();
                    emit(1);
                }).forEach(value => {
                    collected.push(value);
                }).then(() => {
                    assert.deepEqual(collected, []);
                    done();
                });
            });

            test("should not be able to emit for a rejected stream", done => {
                const collected = [];

                new Stream((resolve, reject, emit) => {
                    reject(new Error());
                    emit(1);
                }).forEach(value => {
                    collected.push(value);
                }).catch(() => {
                    assert.deepEqual(collected, []);
                    done();
                });
            });

            test("should return true when called for an unresolved stream", done => {
                new Stream((resolve, reject, emit) => {
                    assert.strictEqual(emit(), true);
                    resolve();
                }).then(done, done);
            });

            test("should return false when called for a resolved stream", done => {
                new Stream((resolve, reject, emit) => {
                    resolve();
                    assert.strictEqual(emit(), false);
                }).then(done, done);
            });

            test("should return false when called for a rejected stream", done => {
                new Stream((resolve, reject, emit) => {
                    reject(new Error());
                    assert.strictEqual(emit(), false);
                }).catch(() => done());
            });
        });
    });

    suite("#flatMap", () => {
        test("should reject when the handler throws", done => {
            let original = new Error();

            Stream.emit(1).flatMap(() => {
                throw original;
            }).catch(err => {
                assert.strictEqual(err, original);
                done();
            });
        });
    });

    suite("#cancel", () => {
        test("should go through derived streams", done => {
            Stream.sleep().then().cancel().then(
                consumed => {
                    assert(consumed);
                }
            ).then(done, done);
        });
    });

    suite(".fromGenerator", () => {
        test("should run", done => {
            Stream.fromGenerator(function*() {
                done();
            });
        });

        test("should return a new Stream instance", done => {
            const stream = Stream.fromGenerator(function*() {
                assert(stream instanceof Stream);
            }).then(done, done);
        });

        test("should forward arguments", done => {
            Stream.fromGenerator(function*(...args) {
                assert.deepEqual(args, [1, 2, 3]);
            }, 1, 2, 3).then(done, done);
        });

        test("should allow sub-streams to succeed", done => {
            Stream.fromGenerator(function*() {
                const result = yield Stream.resolve(1);
                assert.strictEqual(result, 1);
            }).then(done, done);
        });

        test("should allow sub-streams to fail", done => {
            Stream.fromGenerator(function*() {
                const originalErr = new Error();
                try {
                    yield Stream.reject(originalErr);
                } catch (err) {
                    assert.strictEqual(err, originalErr);
                    return;
                }
                throw new Error("not rejected");
            }).then(done, done);
        });

        test("should be cancellable", done => {
            const stream = Stream.fromGenerator(function*() {
                try {
                    yield Stream.sleep(0.0);
                } catch (err) {
                    if (err instanceof Stream.Cancelled) {
                        return;
                    }
                    throw err;
                }
                throw new Error("not cancelled");
            }).then(done, done);

            stream.cancel();
        });

        test("should forward cancellations to sub-streams", done => {
            const sub = Stream.generator(function*() {
                yield Stream.sleep();
            });

            const stream = Stream.fromGenerator(function*() {
                try {
                    yield sub();
                } catch (err) {
                    if (err instanceof Stream.Cancelled) {
                        return;
                    }
                    throw err;
                }
                throw new Error("not cancelled");
            }).then(done, done);

            stream.cancel();
        });

        test("should forward unconsumed cancellations", done => {
            const notCancellable = new Stream(resolve => {
                resolve(null);
            });

            const stream = Stream.fromGenerator(function*() {
                for (let i = 0; i < 100; i++) {
                    yield notCancellable;
                }

                try {
                    yield Stream.sleep();
                } catch (err) {
                    if (err instanceof Stream.Cancelled) {
                        return;
                    }
                    throw err;
                }
                throw new Error("not cancelled");
            }).then(done, done);

            stream.cancel();
        });

        test("should not forward consumed cancellations", done => {
            const stream = Stream.fromGenerator(function*() {
                try {
                    yield Stream.sleep();
                } catch (err) {
                    if (!(err instanceof Stream.Cancelled)) {
                        throw err;
                    }
                }
                yield Stream.sleep(0.0);
            }).then(done, done);

            stream.cancel();
        });

        test("should not consume extra cancellations", done => {
            const stream = Stream.fromGenerator(function*() {
                yield Stream.sleep();
            });

            stream.cancel();
            stream.cancel().then(consumed => {
                assert(!consumed);
            }).then(done, done);
        });
    });
});
