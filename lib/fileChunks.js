import Stream from "./Stream";

export default function fileChunks(file, _chunkSize) {
    const chunkSize = _chunkSize === void 0 ? (1024 * 1024) : _chunkSize;
    const fileSize = file.size;

    return new Stream((resolve, reject, emit) => {
        let offset = 0;
        let reader = new FileReader();

        function next() {
            let blob = file.slice(offset, Math.min(offset + chunkSize, fileSize));
            reader.readAsArrayBuffer(blob);
        }

        function load() {
            let buffer = reader.result;
            offset += buffer.byteLength;

            if (buffer.byteLength > 0) {
                // Note 2015-02-03:
                // Start fetching the next chunk before handling the current one.
                // This appears to parallerize some of the work, at least in Chrome 40.
                next();
                emit({
                    "buffer": buffer,
                    "progress": {
                        "done": offset,
                        "total": fileSize
                    }
                });
            } else {
                resolve();
            }
        }

        function error() {
            reject(reader.error);
        }

        reader.addEventListener("load", load, false);
        reader.addEventListener("error", error, false);

        next();

        return (callback, cancel) => {
            reader.removeEventListener("error", error, false);
            reader.removeEventListener("load", load, false);
            reader.abort();
            callback(reject(cancel));
        };
    });
}
