import Stream from "./Stream";

export default function xhr(method, url, _options) {
    function forEach(obj, func, ctx) {
        if (obj) {
            Object.keys(obj).forEach(key => {
                func.call(ctx, obj[key], key, obj);
            });
        }
    }

    const options = {
        "responseType": "",
        "overrideMimeType": null,
        "payload": "",
        "headers": {}
    };
    forEach(_options, (value, key) => {
        options[key] = value;
    });

    return new Stream((resolve, reject, emit) => {
        let xhr = new XMLHttpRequest();

        let downloadProgress = null;
        let uploadProgress = null;
        function getProgress(progressEvent) {
            if (progressEvent.lengthComputable) {
                return {
                    "done": progressEvent.loaded,
                    "total": progressEvent.total
                };
            }
            return null;
        }
        function emitProgress() {
            emit({
                "downloadProgress": downloadProgress,
                "uploadProgress": uploadProgress
            });
        }

        let downloadHandlers = {
            progress(e) {
                downloadProgress = getProgress(e);
                emitProgress();
            },

            load() {
                let response = xhr.response;
                if (xhr.status === 200) {
                    if (options.responseType === "json") {
                        try {
                            response = JSON.parse(response);
                        } catch (err) {
                            response = null;
                        }
                    }
                }
                resolve({
                    "status": xhr.status,
                    "statusText": xhr.statusText,
                    "response": response
                });
            },

            error() {
                reject("Network error");
            }
        };

        // 2015-04-15: Firefox (37.0.1 at the time of testing) appears to modify
        // its behavior and perform OPTION requests when the XHR instance has
        // upload event listeners. Skip adding the listeners when there is no
        // payload to send.
        let uploadHandlers = {};
        if (options.payload) {
            uploadHandlers = {
                progress(e) {
                    uploadProgress = getProgress(e);
                    emitProgress();
                }
            };
        }

        forEach(downloadHandlers, (handler, name) => {
            xhr.addEventListener(name, handler, false);
        });
        forEach(uploadHandlers, (handler, name) => {
            xhr.upload.addEventListener(name, handler, false);
        });

        xhr.open(method, url);
        forEach(options.headers, (value, key) => {
            xhr.setRequestHeader(key, value);
        });
        xhr.responseType = options.responseType === "json" ? "" : options.responseType;
        // Ensure XMLHttpRequest#overrideMimeType exists, as IE10 does not support it.
        if (options.overrideMimeType !== null && typeof xhr.overrideMimeType === "function") {
            xhr.overrideMimeType(options.overrideMimeType);
        }
        xhr.send(options.payload);

        return (callback, cancel) => {
            forEach(downloadHandlers, (handler, name) => {
                xhr.removeEventListener(name, handler, false);
            });
            forEach(uploadHandlers, (handler, name) => {
                xhr.upload.removeEventListener(name, handler, false);
            });
            xhr.abort();
            callback(reject(cancel));
        };
    });
}
