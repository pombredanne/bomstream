import setImmediate from "bommediate";

export default class Stream {
    constructor(func, ...args) {
        this._result = null;
        this._listeners = [];

        this._cancel = func.call(this,
            value => resolve(this, value),
            err => reject(this, err),
            value => emit(this, value),
            ...args
        );
    }

    flatMap(func, ctx) {
        return wrap(this, defaultResolve, defaultReject, (value, emit) => {
            func.call(ctx, value, emit);
        });
    }

    forEach(func, ctx) {
        return this.flatMap(value => {
            func.call(ctx, value);
        });
    }

    map(func, ctx) {
        return this.flatMap((value, emit) => {
            emit(func.call(ctx, value));
        });
    }

    suppress() {
        return this.flatMap(() => {});
    }

    then(_onResolve, _onReject) {
        let onResolve = _onResolve || defaultResolve;
        let onReject = _onReject || defaultReject;
        return wrap(this, onResolve, onReject, defaultEmit);
    }

    catch(onReject) {
        return this.then(null, onReject);
    }

    cancel(cancelled=null) {
        return new Stream((resolve, reject) => {
            if (!this._cancel) {
                resolve(false);
            } else {
                this._cancel(
                    consumed => {
                        resolve(Boolean(consumed));
                    },
                    cancelled || new Stream.Cancelled()
                );
            }
        });
    }
}

Stream.Cancelled = function(message="cancelled") {
    Error.call(this);

    this.name = "Cancelled";
    this.message = message;
};
Stream.Cancelled.prototype = Object.create(Error.prototype);
Stream.Cancelled.prototype.constructor = Stream.Cancelled;

Stream.resolve = function(value) {
    return asStream(value);
};

Stream.reject = function(err) {
    return new Stream(function(_, reject) {
        reject(err);
    });
};

Stream.emit = function(value) {
    return new Stream(function(resolve, reject, emit) {
        let immediate = setImmediate(() => {
            emit(value);
            resolve();
        });

        return (callback, cancel) => {
            callback(immediate.destroy() && reject(cancel));
        };
    });
};

Stream.sleep = function(milliseconds) {
    if (milliseconds === void 0) {
        return new Stream((resolve, reject, emit) => {
            return (callback, cancel) => {
                callback(reject(cancel));
            };
        });
    }

    return new Stream((resolve, reject, emit) => {
        let timeout = setTimeout(() => {
            timeout = null;
            resolve();
        }, milliseconds);

        return (callback, cancel) => {
            if (timeout === null) {
                callback(false);
            } else {
                clearTimeout(timeout);
                timeout = null;
                callback(reject(cancel));
            }
        };
    });
};

Stream.function = function(func) {
    return function(...args) {
        let bound = () => {
            return func.apply(this, args);
        };
        return new Stream(bound);
    };
};

Stream.generator = function(func) {
    return function(...args) {
        let bound = () => {
            return func.apply(this, args);
        };
        return Stream.fromGenerator(bound);
    };
};

const resolveNull = Stream.resolve(null);

Stream.fromGenerator = function(func, ...args) {
    return new Stream((resolve, reject, emit) => {
        let gen = func(...args);
        let current = resolveNull;
        let currentCancel = resolveNull;
        let cancelQueue = [];

        function tryToCancel(current, cancelQueue) {
            return new Stream(resolve => {
                function next() {
                    if (hasResult(current) || cancelQueue.length === 0) {
                        return resolve();
                    }

                    let { callback, cancel } = cancelQueue[0];
                    current.cancel(cancel).then(consumed => {
                        if (consumed) {
                            cancelQueue.shift();
                            callback(true);
                        }
                        next();
                    });
                }
                next();
            });
        }

        function finish() {
            let oldQueue = cancelQueue;

            gen = null;
            current = null;
            cancelQueue = null;

            return currentCancel.then(() => {
                currentCancel = null;

                oldQueue.forEach(obj => {
                    obj.callback(false);
                });
            });
        }

        function step(_throw, value) {
            let res;
            try {
                if (_throw) {
                    res = gen.throw(value);
                } else {
                    res = gen.next(value);
                }
            } catch (err) {
                return finish().then(() => reject(err));
            }

            if (res.done) {
                return finish().then(() => resolve(res.value));
            }

            currentCancel.then(() => {
                current = asStream(res.value);
                currentCancel = tryToCancel(current, cancelQueue);
                current.forEach(emit).then(
                    value => step(false, value),
                    err => step(true, err)
                );
            });
        }

        setImmediate(step, false, void 0);

        return (callback, cancel) => {
            if (cancelQueue === null) {
                callback(false);
            } else {
                cancelQueue.push({ callback, cancel });
                if (cancelQueue.length === 1) {
                    currentCancel = tryToCancel(current, cancelQueue);
                }
            }
        };
    });
};

function hasResult(stream) {
    return stream._result !== null;
}

function resolve(stream, value) {
    if (hasResult(stream)) {
        return false;
    }
    stream._result = {
        "rejected": false,
        "value": value
    };
    setImmediate(() => {
        let listeners = stream._listeners;
        stream._listeners = null;
        stream._cancel = null;

        listeners.forEach(obj => {
            obj.listener.resolve(value);
        });
    });
    return true;
}

function reject(stream, err) {
    if (hasResult(stream)) {
        return false;
    }
    stream._result = {
        "rejected": true,
        "value": err
    };
    setImmediate(() => {
        let listeners = stream._listeners;
        stream._listeners = null;
        stream._cancel = null;

        listeners.forEach(obj => {
            obj.listener.reject(err);
        });
    });
    return true;
}

function emit(stream, value) {
    if (hasResult(stream)) {
        return false;
    }
    setImmediate(() => {
        stream._listeners.forEach(obj => {
            obj.listener.emit(value);
        });
    });
    return true;
}

function defaultResolve(value, _emit) {
    return value;
}

function defaultReject(err, _emit) {
    throw err;
}

function defaultEmit(value, emit) {
    emit(value);
}

function listen(stream, listener) {
    let listeners = stream._listeners;
    if (listeners !== null) {
        let obj = {
            "index": listeners.length,
            "listener": listener
        };
        stream._listeners.push(obj);
        return obj;
    }

    let result = stream._result;
    if (result.rejected) {
        setImmediate(() => {
            listener.reject(result.value);
        });
    } else {
        setImmediate(() => {
            listener.resolve(result.value);
        });
    }
    return null;
}

function unlisten(stream, obj) {
    if (!obj) {
        return;
    }

    let index = obj.index;
    let listeners = stream._listeners;

    if (index < listeners.length && listeners[index] === obj) {
        let last = listeners.pop();
        if (last !== obj) {
            last.index = index;
            listeners[index] = last;
        }
    }
}

function isThenable(obj) {
    return obj && obj.then !== void 0;
}

function asStream(value) {
    if (value instanceof Stream) {
        return value;
    } else if (isThenable(value)) {
        return new Stream((resolve, reject) => {
            value.then(resolve, reject);
        });
    } else {
        return new Stream(resolve => {
            resolve(value);
        });
    }
}

function wrap(value, onResolve, onReject, onEmit) {
    let stream = asStream(value);

    return new Stream((resolve, reject, emit) => {
        function doReject(_err) {
            let newValue = null;
            try {
                newValue = onReject(_err, emit);
            } catch (err) {
                reject(err);
                return;
            }

            if (isThenable(newValue)) {
                stream = wrap(newValue, resolve, reject, emit);
            } else {
                stream = null;
                resolve(newValue);
            }
        }

        let obj = listen(stream, {
            emit(value) {
                try {
                    onEmit(value, emit);
                } catch (err) {
                    unlisten(stream, obj);
                    doReject(err);
                    return;
                }
            },

            resolve(value) {
                let newValue = null;
                try {
                    newValue = onResolve(value, emit);
                } catch (err) {
                    reject(err);
                    return;
                }

                if (isThenable(newValue)) {
                    stream = wrap(newValue, resolve, reject, emit);
                } else {
                    stream = null;
                    resolve(newValue);
                }
            },

            reject: doReject
        });

        return (callback, cancel) => {
            stream.cancel(cancel).then(consumed => {
                callback(consumed);
            });
        };
    });
}
